from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.forms import ModelChoiceField, ModelForm
from PIL import Image
from django.utils.safestring import mark_safe

from .models import *


class OneroomCategoryChoiceField(forms.ModelChoiceField):
    pass


class TworoomsAdminForm(ModelForm):

   pass


class OneroomAdminForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].help_text = mark_safe('<span style="color: blue;">Загружайте изображения с минимальным разширением {}x{}</span>'.format(*Room.min_resolution))

    def clean_img(self):
        image = self.cleaned_data['image']
        img = Image.open(image)
        min_height, min_width = Room.min_resolution
        max_height, max_width = Room.max_resolution

        if image.size > Room.max_image_size:
            raise ValidationError('Размер изображения не должен превышать 3мб')

        if img.height < min_height or img.width < min_width:
            raise ValidationError('Загруженное изображение меньше допустимого')

        if img.height > max_height or img.width > max_width:
            raise ValidationError('Загруженное изображение больше допустимого')

        return image


class OneroomAdmin(admin.ModelAdmin):

    form = OneroomAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='oneroom'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class TworoomsAdmin(admin.ModelAdmin):

    change_form_template = 'admin.html'
    form = TworoomsAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='tworooms'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(Category)
admin.site.register(Oneroom, OneroomAdmin)
admin.site.register(Tworooms, TworoomsAdmin)
