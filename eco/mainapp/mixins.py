from django.views import View
from django.views.generic.detail import SingleObjectMixin
from .models import Category, Oneroom, Tworooms

class CategoryDetailMixin(SingleObjectMixin):

    CATEGORY_SLUG_TO_ROOM_MODEL = {
        'oneroom': Oneroom,
        'tworooms': Tworooms
    }

    def get_context_data(self, **kwargs):
        if isinstance(self.get_object(), Category):
            model = self.CATEGORY_SLUG_TO_ROOM_MODEL[self.get_object().slug]
            context = super().get_context_data(**kwargs)
            context['categories'] = Category.objects.get_categories_for_left_sidebar()
            context['category_rooms'] = model.objects.all()
            return context
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.get_categories_for_left_sidebar()
        return context
