from django import template
from django.utils.safestring import mark_safe


register = template.Library()

TABLE_HEAD = """
            <table class="table">
              <tbody>
  """
TABLE_TAIL = """
            </tbody>
            </table>
"""

TABLE_CONTENT = """
    <tr>
        <td>{name}</td>
        <td>{value}</td>
    </tr>  
"""

ROOM_SPEC = {
    'oneroom': {
        'Размер номера': 'size',
        'Кровать': 'bed',
        'Ванная': 'bathroom',
        'Кухня': 'kitchen'

    },
    'tworooms': {
        'Размер номера': 'size',
        'Кровать': 'bed',
        'Ванная': 'bathroom',
        'Кухня': 'kitchen'
        }
}

def get_room_spec(room, model_name):
    table_content = ''
    for name, value in ROOM_SPEC[model_name].items():
        table_content += TABLE_CONTENT.format(name=name, value=getattr(room, value))
    return table_content


@register.filter
def room_spec(room):
    model_name = room.__class__._meta.model_name

    return mark_safe(TABLE_HEAD + get_room_spec(room, model_name) + TABLE_TAIL)
