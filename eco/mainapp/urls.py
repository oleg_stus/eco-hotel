from django.urls import path


from .views import (
    BaseView,
    RoomsDetailView,
    CategoryDetailView,
)


urlpatterns = [
    path('', BaseView.as_view(), name='base'),
    path('rooms/<str:ct_model>/<str:slug>/', RoomsDetailView.as_view(), name='detail_rooms'),
    path('category/<str:slug>/', CategoryDetailView.as_view(), name='category_detail'),

]