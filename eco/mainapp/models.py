from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.urls import reverse
from django.utils import timezone



User = get_user_model()


def get_models_for_count(*model_names):
    return [models.Count(model_name) for model_name in model_names]


def get_product_url(obj, viewname):
    ct_model = obj.__class__._meta.model_name
    return reverse(viewname, kwargs={'ct_model': ct_model, 'slug': obj.slug})


class MinResolutionErrorException(Exception):
    pass

class MaxResolutionErrorException(Exception):
    pass


class LatestRoomsManager:

    @staticmethod
    def get_products_for_main_page(*args, **kwargs):
        with_respects_to = kwargs.get('with_respects_to')

        rooms = []
        ct_models = ContentType.objects.filter(model__in=args)
        for ct_model in ct_models:
            model_products = ct_model.model_class()._base_manager.order_by('-id').all()[:5]
            rooms.extend(model_products)
        if with_respects_to:
            ct_model= ContentType.objects.filter(model=with_respects_to)
            if ct_model.exists():
                if with_respects_to in args:
                    return  sorted(rooms, key=lambda x: x.__class__.meta.model_name.startswith(with_respects_to), reverse=True )
        return rooms




class LatestRooms:

    objects = LatestRoomsManager()

class CategoryManager(models.Manager):

    CATEGORY_NAME_COUNT_NAME = {
        'Однокомнатный номер': 'oneroom__count',
        'Двухкомнатный номер': 'tworooms__count'
    }

    def get_queryset(self):
        return super().get_queryset()

    def get_categories_for_left_sidebar(self):
        models = get_models_for_count('oneroom', 'tworooms')
        qs = list(self.get_queryset().annotate(*models))
        data = [
            dict(name=c.name, url=c.get_absolute_url(), count=getattr(c, self.CATEGORY_NAME_COUNT_NAME[c.name]))
            for c in qs
        ]
        return data


class Category(models.Model):

    name = models.CharField(max_length=255, verbose_name='Имя категории')
    slug = models.SlugField(unique=True)
    objects = CategoryManager()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category_detail', kwargs={'slug': self.slug})


class Room(models.Model):

    min_resolution = (400, 400)
    max_resolution = (2000, 2000)
    max_image_size = 3145728

    class Meta:
        abstract = True

    category = models.ForeignKey(Category, verbose_name='Категория', on_delete=models.CASCADE)
    title = models.CharField(max_length=255, verbose_name='Название комнаты')
    slug = models.SlugField(unique=True)
    image = models.ImageField(verbose_name='Изображение')
    description = models.TextField(verbose_name='Описание', null=True)
    price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Цена')

    def __str__(self):
        return self.title

    def get_model_name(self):
        return self.__class__.__name__.lower()

class Oneroom(Room):

    size = models.CharField(max_length=255, verbose_name='Размер номера')
    bed = models.CharField(max_length=255, verbose_name='Кровать')
    bathroom = models.CharField(max_length=255, verbose_name='Ванная комната')
    kitchen = models.CharField(max_length=255, verbose_name='Кухня')



    def __str__(self):
        return f'{self.category.name} : {self.title}'

    def get_absolute_url(self):
        return get_product_url(self, 'detail_rooms')




class Tworooms(Room):

    size = models.CharField(max_length=255, verbose_name='Размер')
    bed = models.CharField(max_length=255, verbose_name='Кровать')
    bathroom = models.CharField(max_length=255, verbose_name='Ванная комната')
    kitchen = models.CharField(max_length=255, verbose_name='Кухня')


    def __str__(self):
        return f'{self.category.name} : {self.title}'

    def get_absolute_url(self):
        return get_product_url(self, 'detail_rooms')


