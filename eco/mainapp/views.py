from django.shortcuts import render
from django.views.generic import DetailView
from django.views import View
from .mixins import CategoryDetailMixin
from .models import Category, Tworooms, Oneroom, LatestRooms


class BaseView(View):

    def get(self, request, *args, **kwargs):
        categories = Category.objects.get_categories_for_left_sidebar()
        rooms = LatestRooms.objects.get_products_for_main_page('oneroom', 'tworooms')
        context = {
            'categories': categories,
            'rooms': rooms,

        }
        return render(request, 'base.html', context)


class RoomsDetailView(CategoryDetailMixin, DetailView):

    ct_model_model_class = {
        'oneroom': Oneroom,
        'tworooms': Tworooms,
    }

    def dispatch(self, request, *args, **kwargs):

        self.model = self.ct_model_model_class[kwargs['ct_model']]
        self.queryset = self.model._base_manager.all()
        return super().dispatch(request, *args, **kwargs)

    context_object_name = 'room'
    template_name = 'detail_rooms.html'
    slug_url_kwarg = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ct_model'] = self.model._meta.model_name
        return context


class CategoryDetailView(CategoryDetailMixin, DetailView):

    model = Category
    queryset = Category.objects.all()
    context_object_name = 'category'
    template_name = 'category_detail.html'
    slug_url_kwarg = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context



